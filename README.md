# Let's Show Our Gratitude To Dev Bootcamp (The Great Dev Bootcamp Photogrammetry Project ~ VR-Dev-Bootcamp)

## Introduction
Remember how Dev Bootcamp taught us to "Make beautiful and meaningful things"?

In the light of that, Bernice has an idea of how to preserve Dev Bootcamp in at least some form after it is gone.

There's a technology called photogrammetry.

Basically, photogrammetry means taking tons and tons of photos from different angles and feed it into software that will stitch it together. Bernice recently did that project to make a VR version of the Internet Archive (see https://gitlab.com/unityversity/InternetVARchive).  The team that she was in consisted of less than 10 people, and they worked on it for less than half a week, and it was only at night, so that they won't get in the way of the people who work in the Internet Archive.

Since Bernice is already paying for the software, we might as well use it and make a VR version of ALL the Dev Bootcamps!! ^___^ https://gitlab.com/BerniceChua/VR-Dev-Bootcamp

This would be a collaborative project, to preserve something that has been so meaningful to us -- the alumni who have benefited so much from Dev Bootcamp.  Who's with me???? =D  https://gitlab.com/BerniceChua/VR-Dev-Bootcamp

(Bernice really regrets that she didn't know about photogrammetry being accessible earlier so that the time is not so hectic.  But let's work with what we've got.)


## What is Photogrammetry?
Photogrammetry means taking tons and tons of photos of something, from various angles, then feeding all the photos into software that will stitch everything together to turn them into a computer graphics 3D model.

For more details, please see: [Photogrammetry - A Way To Create Virtual Reality Worlds](https://github.com/BerniceChua/Adventures_In_Game-Development/blob/master/02_Photogrammetry_-_A_Way_To_Create_Virtual_Reality_Worlds_1.md)

The software being used in this case is called [RealityCapture](http://store.steampowered.com/app/489180/RealityCapture).  RealityCapture costs either $39.99/month or $109.99 for 3 months, unless Steam has a sale.
###### Since this is expensive, and Bernice is already paying for it, why don't we photogrammetry Dev Bootcamp??

###### [Please join this open-source project: "The Great Dev Bootcamp Photogrammetry Project"](https://gitlab.com/BerniceChua/VR-Dev-Bootcamp)


### How To Help:
![I need you to contribute to this open source project](https://i.imgflip.com/20impt.jpg)

- Join and contribute to this project! ^_^
- If you cannot join this project, please share/announce this project in as many ways as you can:
	- the more people who know about this, the likelier it is that someone will be able to help.  And the more people who help, the less photography each person needs to do.
		- email all the alumni and staff that you know
		- send SMS to everyone you know
		- share this on facebook
		- share this on LinkedIn
		- share this on Twitter
		- share this on Google+
		- share this on Tumblr
		- share this on Reddit


### How To Communicate / Collaborate / Join / Let Bernice Know That You Have Joined:
- Join the Discord server -- https://discord.gg/MCWVgfB
- Join #dbc-vr-project in either reply-all.slack.com or dbc-sf.slack.com
- This sign up sheet might help for coordinating: https://docs.google.com/spreadsheets/d/19m0y7lne4WlwuNIwsJHg_oJaRyb9WUkmcFQ929jgZRc/edit?usp=sharing
- You can use this "Google Drawings" to track which areas have already been covered: https://docs.google.com/drawings/d/1GGpxtahbm98OMobPDBpBDoaAdpBlVAbSbAjfz6lXXEQ/edit?usp=sharing


## How To Contribute Photos:
For technical people:
- Create an account in GitLab <https://gitlab.com/>.  (You can use your GitHub login to OAuth into GitLab, if you don't want to create a new login.)

For both technical & non-technical people:
- Email the photo(s) to Bernice - Bernice.Chua.415@gmail.com
- If you have joined the Discord server, you can upload your pictures there.  If you're doing it this way, please put your full name to receive full credit.


## What Will Happen To The Results
When this project is done, it will be posted in itch.io (and possibly Steam) for free.  Bernice is also learning how to do WebVR, so this could go into a web browser.  Once multiplayer is implemented (further down the line), you can treat this like a "virtual office space" or "virtual meeting space".


## What's In It For You, If You Come In And Take Photos:
- You would participated in something AWESOME!!!!
- Your name will live on in eternity as someone who contributed to this project.
- This is an open source project.  It looks good on your resume to contribute to an open source project.
- Bernice will teach you everything that she knows about making videogames, VR, AR, and the things that she has recently learned about AI.  Like a private tutor.
- When you are done, Bernice will invite you to the Discord server of Chua Productions, where your opinions can shape the next game that she makes!!  You can vote on different things and give feedback, etc.
- You can be in one of Bernice's future videogames.  You can choose - your likeness (Bernice will photogrammetry you), your voice (voice acting for the game), your name (a character will be named after you).  You can choose all of them or if you're uncomfortable with your likeness being in a game, you can choose to skip that and only have a character named after you, and/or only do voice acting.  Let's talk about it.
- You get to hang out with your friends from DBC while photogrammetrying DBC! :D
- Bernice will feed you.  (Please let me know which day and time you will arrive, so I can buy the right amount of food.  Also, please give your food allergies or dietary restrictions.)
- When the project is completed, you would be able to visit DBC any time that you want!!


### How To Photogrammetry
![One does not simply use any random photo. (There's an art to this.)](./PhotogrammetryOneDoesNotSimply20imdb.jpg)

VIDEO: https://www.youtube.com/watch?v=8HuOvf4rKaw
- Ideal method for rooms: https://pix4d.com/wp-content/uploads/2017/02/reconstruction-3-768x699.jpg
- Ideal method for narrow hallways: https://pix4d.com/wp-content/uploads/2017/02/acquisition-orientation-1-768x285.jpg
(Sources: https://pix4d.com/indoor-mapping-game-plan/ )

##### Where To Stand When Photogrammetry-ing A Large Room:
![Where To Stand When Photogrammetry-ing A Large Room](./reconstruction-3-768x699.jpg)


##### Where To Stand When Photogrammetry-ing A Narrow Hallway:
![Where To Stand When Photogrammetry-ing A Narrow Hallway](./acquisition-orientation-1-768x285.jpg)



### What's needed:
1. (optional) Watch the instructional video: https://www.youtube.com/watch?v=8HuOvf4rKaw
2. Get your camera.  Even the one in your phone is good for this project.
3. Go to your local DBC campus.
4. If you need a refresher on how to photogrammetry, get Bernice to show you.
5. Pick the area that you want to stand in to photograph.
	- This sign up sheet might help for coordinating: https://docs.google.com/spreadsheets/d/19m0y7lne4WlwuNIwsJHg_oJaRyb9WUkmcFQ929jgZRc/edit?usp=sharing
	- You can use this "Google Drawings" to track which areas have already been covered: https://docs.google.com/drawings/d/1GGpxtahbm98OMobPDBpBDoaAdpBlVAbSbAjfz6lXXEQ/edit?usp=sharing
6. Take the picture(s).
7. Upload your picture(s).
	- 7a. Uploading for technical people:
		- 7a1. Go to https://gitlab.com/BerniceChua/VR-Dev-Bootcamp/
		- 7a2. Clone the repo at https://gitlab.com/BerniceChua/VR-Dev-Bootcamp/
		- 7a3. Create a new branch, so that you can push your changes.
		- 7a4. Make a pull request of the photo that you took.  Bernice will merge the changes (add them to the project).
	- 7b. Uploading for non-technical people who want to help out -- please email your photos to Bernice.Chua.415@gmail.com or upload your pictures to the Discord server
8. Eat some food and hang out with your friends who came to participate that day.
9. That's all.
10. Seriously, that's all.



## FAQ:
- Can I really use the camera of my cellphone?
	- Yes.

- Why GitLab instead of GitHub?
	- Because GitLab allows larger individual files to be uploaded.

- Why Discord?
	- Because it's not a memory hog like Slack, and it has better voice/video/screensharing for collaborative work.

- Why are you doing this?
	- Why not?  OK, seriously, it's for showing gratitude to DBC, and it's for preserving something that's been so meaningful.  Let's face it, 2016 & 2017 had lots of crappy things and crappy events, so it's just nice to save something that's good.  Also, the U.S. is the only country that I know of where a president gave a speech that said "We do things not because they are easy, but because they are hard."  It's an excerpt from President JFK's speech about going to the moon.  I really believe in that.

- When can I go do this?
	- Everyday and every time until and including December 8.  December 8 is a hard deadline, because that's actually when DBC will stop existing.  In the evenings, after you leave work, Bernice will be upstairs to open the door for you.  (FYI: On December 7, Bernice will go to this event (please go there too): https://act.demandprogress.org/event/team-internet/1374 but she will return to DBC when this event is over.)



I don't use facebook, so please share this information there.  Disseminating this information is another way that you can help.  The more people who know about  this, the more it's likely that someone who sees it will be able to help.  (I already spread this message in LinkedIn, Google+, and Twitter, but feel free to share in those platforms too.)  And the more people who participate, the less work each person needs to do.

Thank you so much for your time and your help! ^_^  Please let Bernice know if you have any questions.


## If each of us just contributed a small part, then I believe that this can be achievable!! \^__^  The photogrammetry for the Internet Archive project was done in half a week.  We have more time than half a week.  Thanks in advance for all of your help! \^\_\_\_\^
Let's do this, so that Dev Bootcamp can at least exist in some form even after it is "gone".

### If you want to contribute more than the minimum:
- If you already use Unity, Meshlab, & Zephyr, or any of these tools, please contact Bernice through the links below.
	- If you don't have Unity, but you would like to learn, please see: [Adventures In Game-Development](https://github.com/BerniceChua/Adventures_In_Game-Development)


## Roadmap
- What Will Happen To The Results
    - When this project is done, it will be posted in itch.io (and possibly Steam) for free.  Bernice is also learning how to do WebVR, so this could go into a web browser.  Once multiplayer is implemented (further down the line), you can treat this like a "virtual office space" or "virtual meeting space".
- future plans: you can watch the videos of the DBC talks and the graduations.  you can write on the whiteboards' surfaces.

## Contributor List (in alphabetical order) - Who Made This?
- Bernice Anne W. Chua (game developer - programming, game design, UX/UI, art direction, music)
  - [GitHub](https://github.com/BerniceChua)
  - [LinkedIn](https://linkedin.com/in/bernicechua415)
  - [Twitter](https://twitter.com/ChuaBernice)
- Joseph Lesca
	- [GitHub](https://github.com/jlesca1)
	- [LinkedIn](https://www.linkedin.com/in/josephlesca/)



## Feedback
We welcomes any and all the feedbacks! ^_^  You can use the above links to contact the people involved in this project.

OR

Please send feedback by [creating a new Issue](https://gitlab.com/BerniceChua/VR-Dev-Bootcamp/issues/new), or by [clicking on the Issues link on the sidebar](https://gitlab.com/BerniceChua/VR-Dev-Bootcamp/issues).  Feedback can also be sent to Bernice through the links under her name in the contributor list.
